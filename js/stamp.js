Vue.use(window["vue-js-modal"].default)

var vmMap = new Vue({
    el: '#map',
    data: {
        map: {
            center: {
                lat: 35.258000,
                lng: 139.664497
            },
            zoom: 11.6,
        }
    },
    methods: {
        // google map の描画
        initMap: function () {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: this.map.center,
                zoom: this.map.zoom
            });

            for (var i = 0; i < vm.stamps.length; i++) {
                markerLatLng = new google.maps.LatLng({
                    lat: vm.stamps[i]['lat'],
                    lng: vm.stamps[i]['lon']
                }); // 緯度経度のデータ作成
                var marker = new google.maps.Marker({
                    position: markerLatLng,
                    map: map,
                    title: vm.stamps[i]['name'],
                    icon: {
                        fillColor: "#3333dd", //塗り潰し色
                        fillOpacity: 0.7, //塗り潰し透過率
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: 7, //円のサイズ
                        strokeColor: "#3333ff", //枠の色
                        strokeWeight: 0.8 //枠の透過率
                    },
                    label: {
                        text: i + 1 + "",
                        color: "white",
                    }
                });

                marker.addListener('click', function () {
                    var url = 'https://www.google.co.jp/maps?q=' + this.position.lat() + ',' + this.position.lng();
                    open(url, "_blank");
                });
            }
        }
    },
});

var vm = new Vue({
    el: '#main-contents',
    data: {
        tabs: [{name: 'ALL', id: 9}, {name: '房総半島', id: 0}, {name: '三浦半島', id: 1}],
        currentPrize: null,
        currentTabId: 9,
        userStamps: [
            {
                "id": "1",
                "bookmark_id": 48201,
                "get_at": "2020-01-25 19:45:23",
                "used_at": null,
                "access_log_id": "165"
            },
            {
                "id": "2",
                "bookmark_id": 48204,
                "get_at": "2020-01-25 19:45:31",
                "used_at": null,
                "access_log_id": "166"
            },
        ],
        userPrizes: [
            {
                "id": 1,
                "prize_id": 4,
                "seq_no": 0,
                "get_at": "2020-01-25 19:46:01",
                "used_at": null,
                "token": "78b8a4561e"
            },
            {
                "id": 2,
                "prize_id": 3,
                "seq_no": 0,
                "get_at": "2020-01-25 19:46:37",
                "used_at": null,
                "token": "153201d0d3"
            },
            {
                "id": 3,
                "prize_id": 1,
                "seq_no": 0,
                "get_at": "2020-01-25 19:49:25",
                "used_at": null,
                "token": "a5022649ff"
            }
        ],
        stamps: [
            {
                "bookmark_id": 48200,
                "name": "道の駅　とみうら枇杷倶楽部",
                "disp_name": "",
                "sub_name": "",
                "image_url": "img/stamp02.png",
                "description": "テストテストテストテストテストテスト",
                "lat": 35.250144,
                "lon": 139.703015,
                "distance": 300,
                "is_navigator": false,
                "release_interval": 0,
                "group_tag": 0
            },
            {
                "bookmark_id": 48201,
                "name": "白浜オーシャンリゾート",
                "disp_name": "",
                "sub_name": "",
                "image_url": "img/stamp02.png",
                "description": "営業時間：10:00-12:00",
                "lat": 35.230144,
                "lon": 139.703015,
                "distance": 300,
                "is_navigator": false,
                "release_interval": 0,
                "group_tag": 0
            },
            {
                "bookmark_id": 48202,
                "name": "三浦市観光インフォメーションセンター",
                "disp_name": "",
                "sub_name": "",
                "image_url": "img/stamp02.png",
                "description": "",
                "lat": 35.230144,
                "lon": 139.693015,
                "distance": 300,
                "is_navigator": false,
                "release_interval": 0,
                "group_tag": 1
            },
            {
                "bookmark_id": 48203,
                "name": "世界三代記念艦　三笠",
                "disp_name": "",
                "sub_name": "",
                "image_url": "img/stamp02.png",
                "description": "",
                "lat": 35.230144,
                "lon": 139.683015,
                "distance": 300,
                "is_navigator": false,
                "release_interval": 0,
                "group_tag": 1
            },
            {
                "bookmark_id": 48204,
                "name": "横須賀しょうぶ園",
                "disp_name": "",
                "sub_name": "",
                "image_url": "img/stamp02.png",
                "description": "",
                "lat": 35.230144,
                "lon": 139.663015,
                "distance": 300,
                "is_navigator": false,
                "release_interval": 0,
                "group_tag": 1
            },
        ],
        prizes: [
            {
                "prize_id": 1,
                "name": "コンプリート",
                "condition": "",
                "end_status": 0
            },
            {
                "prize_id": 2,
                "name": "千葉コンプリート",
                "condition": "",
                "end_status": 0
            },
            {
                "prize_id": 3,
                "name": "横須賀コンプリート",
                "condition": "",
                "end_status": 0
            },
            {
                "prize_id": 4,
                "name": "参加賞",
                "condition": "",
                "end_status": 0
            }
        ],
    },
    methods: {
        hasStamp(stamp) {
            return this.userStamps.filter(userStamp => userStamp.bookmark_id === stamp.bookmark_id).length > 0
        },
        hasPrize(prize) {
            return this.userPrizes.filter(userPrize => userPrize.prize_id === prize.prize_id).length > 0
        },
        show: function (name) {
            this.$modal.show(name);
        },
        hide: function (name) {
            this.$modal.hide(name);
        },
        showPrizeModal(prize) {
            this.currentPrize = prize
            this.show('prize');
        }
    },
    computed: {
        currentDispStamps() {
            return (this.currentTabId === 9) ? this.stamps : this.stamps.filter(stamp => stamp.group_tag === this.currentTabId)
        },
        stampSheetClasses() {
            switch (this.currentTabId) {
                case 9:
                    return 'stamp-sheet-all';
                case 0:
                    return 'stamp-sheet-bouso';
                case 1:
                    return 'stamp-sheet-miura';
                default:
                    return;
            }
        }
    },
    // created: function () {
    //     axios.post('./api/stamps_get.php').then(function (response) {
    //         console.log(response.data);
    //     }).catch(function (error) {
    //         console.log(error);
    //     }).finally(function () {
    //     })
    // }
})
